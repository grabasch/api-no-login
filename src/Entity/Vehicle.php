<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use App\Repository\VehicleRepository;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: VehicleRepository::class)]
#[ApiResource(
    operations: [
        new Get(
            normalizationContext: ['groups'=> ['vehicle:read', 'vehicle:item:get']]
        ),
        new GetCollection(),
        new Put(),
        new Post(),
        new Delete()
    ],
    paginationItemsPerPage: 3,
    formats: ['jsonld', 'json', 'html', 'jsonhal', 'csv' => ['text/csv']],
    normalizationContext: ["groups" => ['vehicle:read']],
    denormalizationContext: ["groups" => ['vehicle:write']]
)]

#[ApiFilter(BooleanFilter::class, properties: ['removed'])]
#[ApiFilter(SearchFilter::class, properties: ['name' => 'partial'])]

class Vehicle
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['vehicle:read'])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank]
    #[Assert\Length(['min' => 2, 'maxMessage' => "Bitte verwende weniger als 50 Zeichen", 'max' => 50 ])]
    #[Groups(['vehicle:write','vehicle:write','user:read'])]
    private ?string $name = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['vehicle:write','vehicle:write'])]
    private ?string $license_plate = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['vehicle:write','vehicle:write','user:read'])]
    private ?string $typ = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    #[Groups(['vehicle:write','vehicle:write'])]
    private ?\DateTimeInterface $created = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(['vehicle:write','vehicle:write'])]
    private ?\DateTimeInterface $buyDate = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 10, scale: 2, nullable: true)]
    #[Groups(['vehicle:write','vehicle:write'])]
    private ?string $CostAsNew = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(['vehicle:write','vehicle:write'])]
    private ?string $description = null;

    #[ORM\Column(nullable: true)]
    #[Groups(['vehicle:write','vehicle:write'])]
    private ?int $expectedMileage = null;

    #[ORM\Column(nullable: true)]
    #[Groups(['vehicle:write','vehicle:write'])]
    private ?int $expectedMaxAge = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    # Das Datum an dem das Fahrzeug hergestellt wurde
    #[Groups(['vehicle:write','vehicle:write'])]
    private ?\DateTimeInterface $constructionYear = null;

    #[ORM\Column(nullable: true)]
    #[Groups(['vehicle:write','vehicle:write'])]
    private ?bool $removed = null;

    #[ORM\ManyToOne(inversedBy: 'vehicles')]
    #[Groups(['vehicle:read','vehicle:write'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $owner = null;

    public function __construct(string $name)
    {
        $this->name = $name;
        $this->created = new \DateTime();
        $this->removed = false;
    }

    public function getId(): ?int
    {
        return $this->id;
    }
    #[Groups('user.read')]
    public function getName(): ?string
    {
        return $this->name;
    }
    #[Groups('user.read')]
    public function getLicensePlate(): ?string
    {
        return $this->license_plate;
    }

    public function setLicensePlate(?string $license_plate): self
    {
        $this->license_plate = $license_plate;

        return $this;
    }

    public function getTyp(): ?string
    {
        return $this->typ;
    }

    public function setTyp(string $typ): self
    {
        $this->typ = $typ;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getBuyDate(): ?\DateTimeInterface
    {
        return $this->buyDate;
    }

    public function setBuyDate(?\DateTimeInterface $buyDate): self
    {
        $this->buyDate = $buyDate;

        return $this;
    }

    public function getCostAsNew(): ?string
    {
        return $this->CostAsNew;
    }

    public function setCostAsNew(?string $CostAsNew): self
    {
        $this->CostAsNew = $CostAsNew;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getExpectedMileage(): ?int
    {
        return $this->expectedMileage;
    }

    public function setExpectedMileage(?int $expectedMileage): self
    {
        $this->expectedMileage = $expectedMileage;

        return $this;
    }

    public function getExpectedMaxAge(): ?int
    {
        return $this->expectedMaxAge;
    }

    public function setExpectedMaxAge(?int $expectedMaxAge): self
    {
        $this->expectedMaxAge = $expectedMaxAge;

        return $this;
    }

    public function getConstructionYear(): ?\DateTimeInterface
    {
        return $this->constructionYear;
    }

    public function setConstructionYear(?\DateTimeInterface $constructionYear): self
    {
        $this->constructionYear = $constructionYear;

        return $this;
    }

    public function isRemoved(): ?bool
    {
        return $this->removed;
    }

    public function setRemoved(?bool $removed): self
    {
        $this->removed = $removed;

        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }
}
